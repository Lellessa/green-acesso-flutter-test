import 'package:flutter/material.dart';

class UsersBloc extends ChangeNotifier {
  List users = [];

  setUsers(List newUsers) {
    this.users = newUsers;
    notifyListeners();
  }
}