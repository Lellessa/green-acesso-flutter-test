import 'package:flutter/material.dart';
import 'package:flutter_app/blocs/users_bloc.dart';
import 'package:flutter_app/colors.dart';
import 'package:flutter_app/views/all_users/all_users.dart';
import 'package:flutter_app/views/common_widgets/top_rate_card.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:provider/provider.dart';

class TopRates extends StatefulWidget {

  @override
  _TopRatesState createState() => _TopRatesState();
}

class _TopRatesState extends State<TopRates> {

  Future _future;
  final String apiUrl = 'https://api.github.com/users';

  fetchUsers() async {
    http.Response response = await http.get(apiUrl);
    List jsonResponse = json.decode(response.body);
    return jsonResponse;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    // Future dentro do initState para economizar consumo de dados
    this._future = fetchUsers();
  }

  @override
  Widget build(BuildContext context) {

    // Bloc
    UsersBloc usersBloc = Provider.of<UsersBloc>(context);

    return FutureBuilder(
      future: this._future, 
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {

          // null data verification
          case ConnectionState.none: 
            return Container(
              color: white,
              height: MediaQuery.of(context).size.height,
              child: Text('Sem dados', style: TextStyle(fontSize: 20, color: black)),
            );

          // Loading
          case ConnectionState.waiting:
            return Container(
              color: white,
              height: MediaQuery.of(context).size.height,
              child: Text('Carregando...', style: TextStyle(fontSize: 20, color: black))
            );

          // Fetch data
          case ConnectionState.done:
          case ConnectionState.active:

            // Future para o notify Listener do Bloc conseguir rodar dentro do Build
            Future.delayed(Duration(seconds: 0)).then((_) {
              usersBloc.setUsers(snapshot.data);
            });

            return Container(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [

                  // Texts
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Top rate', style: TextStyle(fontSize: 22, color: black, fontWeight: FontWeight.w500),),

                        // Button to all users page
                        GestureDetector(
                          onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context)=>AllUsersPage())),
                          child: Text('See all', style: TextStyle(fontSize: 18, color: black.withOpacity(0.6), fontWeight: FontWeight.w500)),
                        ),
                      ],
                    ),
                  ),

                  SizedBox(height: 15),

                  // Cards
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: ListView.builder(

                      // Limite de 10 users para economizar consumo de dados
                      itemCount: 10,
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,

                      itemBuilder: (BuildContext context, int i) {

                        //Gettinf the user
                        var user = snapshot.data[i];
                        
                        return TopRateCard(
                          imgUrl: user['avatar_url'],
                          name: user['login'],
                          id: user['id'],
                          htmlUrl: user['html_url'],
                        );
                      },
                    ),
                  ),

                ],
              ),
            );
          default:
            return Text('DEFAULT');
        }
      },
    );
  }
}