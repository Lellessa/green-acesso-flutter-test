import 'package:flutter/material.dart';
import 'package:flutter_app/colors.dart';
import 'package:flutter_app/views/common_widgets/app_bar.dart';
import 'package:flutter_app/views/home/cards/search_bar.dart';
import 'package:flutter_app/views/home/cards/title.dart';
import 'package:flutter_app/views/home/categories.dart';
import 'package:flutter_app/views/home/top_rates.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  final double _marginY = 25;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: blue,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [

            // AppBar
            MainAppBar(),

            // Body
            Expanded(
              child: SingleChildScrollView(
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: backgroundColor,
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(30), topRight: Radius.circular(30))
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [

                      // Title and Search Bar
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [

                            SizedBox(height: _marginY),

                            // Title
                            MainTitle(),

                            SizedBox(height: _marginY),

                            // Search Bar
                            MainSearchBar(),

                          ],
                        ),
                      ),

                      SizedBox(height: _marginY),
                      // Category
                      Categories(),

                      SizedBox(height: _marginY),
                      // Top Rates
                      TopRates(),

                    ],
                  ),
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }
}
