import 'package:flutter/material.dart';
import 'package:flutter_app/colors.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CategoryCard extends StatelessWidget {
  final String icon, category, qtd;
  CategoryCard({this.icon, this.category, this.qtd});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.28,
      margin: EdgeInsets.symmetric(horizontal: 10),
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
      decoration: BoxDecoration(
        color: green,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        children: [

          SvgPicture.asset(this.icon, color: white, height: 40,),

          SizedBox(height: 15),
          Text(this.category, style: TextStyle(fontSize: 20, color: white, fontWeight: FontWeight.bold), maxLines: 1, overflow: TextOverflow.ellipsis,),

          SizedBox(height: 8),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
            decoration: BoxDecoration(
              color: lightGreen,
              borderRadius: BorderRadius.circular(10)
            ),
            child: Text(
              '${this.qtd} Programmers', 
              overflow: TextOverflow.ellipsis,
              maxLines: 2, 
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 10, color: white), 
            ),
          ),

        ],
      ),
    );
  }
}