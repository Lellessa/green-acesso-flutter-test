import 'package:flutter/material.dart';
import 'package:flutter_app/colors.dart';
import 'package:flutter_app/views/common_widgets/user_img.dart';

class MainAppBar extends StatelessWidget {

  final bool hasBackButton;

  MainAppBar({this.hasBackButton = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [

          // Back Button verify
          this.hasBackButton ? GestureDetector(
            onTap: () => Navigator.pop(context),
            child: Icon(Icons.keyboard_arrow_left, size: 35, color: white,),
          ) : Icon(Icons.menu, size: 30, color: white,),
          Row(
            children: [

              // Notification Icon
              Icon(Icons.notifications, size: 30, color: white,),

              SizedBox(width: 10),

              // User image
              UserImage(),
            ],
          ),
        ],
      ),
    );
  }
}